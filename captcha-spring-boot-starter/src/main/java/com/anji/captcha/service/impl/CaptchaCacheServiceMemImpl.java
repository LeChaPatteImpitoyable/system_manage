package com.anji.captcha.service.impl;

import com.anji.captcha.service.CaptchaCacheService;
import com.ruoyi.common.core.redis.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.TimeUnit;

/**
 * 对于分布式部署的应用，我们建议应用自己实现CaptchaCacheService，比如用Redis，参考service/spring-boot代码示例。
 * 如果应用是单点的，也没有使用redis，那默认使用内存。
 * 内存缓存只适合单节点部署的应用，否则验证码生产与验证在节点之间信息不同步，导致失败。
 * @Title: 默认使用内存当缓存
 * @author lide1202@hotmail.com
 * @date 2020-05-12
 */
public class CaptchaCacheServiceMemImpl implements CaptchaCacheService {
    @Autowired
    private RedisCache redisCache;

    @Override
    public void set(String key, String value, long expiresInSeconds) {
        redisCache.setCacheObject(key, value, expiresInSeconds, TimeUnit.SECONDS);
    }

    @Override
    public boolean exists(String key) {
        return redisCache.getCacheObject(key) != null;
    }

    @Override
    public void delete(String key) {
        redisCache.expire(key, 0);
    }

    @Override
    public String get(String key) {
        return redisCache.getCacheObject(key);
    }

    @Override
    public String type() {
        return "local";
    }
}
