package com.ruoyi.student.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 学员标签对象 stu_label
 *
 * @author ruoyi
 * @date 2020-10-09
 */
public class StuLabel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标签id */
    private Long labelId;

    /** 学员id */
    @Excel(name = "学员id")
    private Long sId;

    /** 标签名称 */
    @Excel(name = "标签名称")
    private String labelName;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setLabelId(Long labelId)
    {
        this.labelId = labelId;
    }

    public Long getLabelId()
    {
        return labelId;
    }
    public void setsId(Long sId)
    {
        this.sId = sId;
    }

    public Long getsId()
    {
        return sId;
    }
    public void setLabelName(String labelName)
    {
        this.labelName = labelName;
    }

    public String getLabelName()
    {
        return labelName;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("labelId", getLabelId())
            .append("sId", getsId())
            .append("labelName", getLabelName())
            .append("delFlag", getDelFlag())
            .append("dataAttDept", getDataAttDept())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
