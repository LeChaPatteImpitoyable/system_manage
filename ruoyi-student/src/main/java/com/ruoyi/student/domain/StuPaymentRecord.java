package com.ruoyi.student.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 缴费记录对象 stu_payment_record
 *
 * @author yingsy
 * @date 2020-10-16
 */
public class StuPaymentRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 记录id */
    private Long recordId;

    /** 学员编号 */
    @Excel(name = "学员编号")
    private Long sId;

    /** 缴费方式：stu_payment_form */
    @Excel(name = "缴费方式：stu_payment_form")
    private String paymentForm;

    /** 支付方式：stu_payment_method */
    @Excel(name = "支付方式：stu_payment_method")
    private String paymentMethod;

    /** 收取金额 */
    @Excel(name = "收取金额")
    private BigDecimal chargeAmount;

    /** 支付时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "支付时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date paymentTime;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setRecordId(Long recordId)
    {
        this.recordId = recordId;
    }

    public Long getRecordId()
    {
        return recordId;
    }
    public void setsId(Long sId)
    {
        this.sId = sId;
    }

    public Long getsId()
    {
        return sId;
    }
    public void setPaymentForm(String paymentForm)
    {
        this.paymentForm = paymentForm;
    }

    public String getPaymentForm()
    {
        return paymentForm;
    }
    public void setPaymentMethod(String paymentMethod)
    {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod()
    {
        return paymentMethod;
    }
    public void setChargeAmount(BigDecimal chargeAmount)
    {
        this.chargeAmount = chargeAmount;
    }

    public BigDecimal getChargeAmount()
    {
        return chargeAmount;
    }
    public void setPaymentTime(Date paymentTime)
    {
        this.paymentTime = paymentTime;
    }

    public Date getPaymentTime()
    {
        return paymentTime;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("recordId", getRecordId())
            .append("sId", getsId())
            .append("paymentForm", getPaymentForm())
            .append("paymentMethod", getPaymentMethod())
            .append("chargeAmount", getChargeAmount())
            .append("paymentTime", getPaymentTime())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("dataAttDept", getDataAttDept())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
