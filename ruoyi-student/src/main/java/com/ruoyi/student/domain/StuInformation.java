package com.ruoyi.student.domain;

import com.ruoyi.common.annotation.Excels;
import com.ruoyi.common.core.domain.entity.SysDept;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 学员信息对象 stu_information
 *
 * @author ying
 * @date 2020-10-15
 */
public class StuInformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 学员id */
    private Long sId;

    /** 姓名 */
    @Excel(name = "姓名")
    private String sName;

    /** 性别 */
    @Excel(name = "性别")
    private String sSex;

    /** 年龄 */
    @Excel(name = "年龄")
    private String sAge;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String sPhone;

    /** 身份证件号码 */
    @Excel(name = "身份证件号码")
    private String sNumid;

    /** 舞蹈考级 */
    @Excel(name = "舞蹈考级")
    private String sExam;

    /** 剩余学期 */
    @Excel(name = "剩余学期")
    private String sRemainderSemester;

    /** 学员状态 */
    @Excel(name = "学员状态")
    private String sStatus;

    /** 所属班级 */
    @Excel(name = "所属班级")
    private Long sAttClassId;

    /** 部门对象 */
    @Excels({
            @Excel(name = "部门名称", targetAttr = "deptName", type = Excel.Type.EXPORT),
            @Excel(name = "部门负责人", targetAttr = "leader", type = Excel.Type.EXPORT)
    })
    private SysDept dept;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public SysDept getDept() {
        return dept;
    }

    public void setDept(SysDept dept) {
        this.dept = dept;
    }

    public void setsId(Long sId)
    {
        this.sId = sId;
    }

    public Long getsId()
    {
        return sId;
    }
    public void setsName(String sName)
    {
        this.sName = sName;
    }

    public String getsName()
    {
        return sName;
    }
    public void setsSex(String sSex)
    {
        this.sSex = sSex;
    }

    public String getsSex()
    {
        return sSex;
    }
    public void setsAge(String sAge)
    {
        this.sAge = sAge;
    }

    public String getsAge()
    {
        return sAge;
    }
    public void setsPhone(String sPhone)
    {
        this.sPhone = sPhone;
    }

    public String getsPhone()
    {
        return sPhone;
    }
    public void setsNumid(String sNumid)
    {
        this.sNumid = sNumid;
    }

    public String getsNumid()
    {
        return sNumid;
    }
    public void setsExam(String sExam)
    {
        this.sExam = sExam;
    }

    public String getsExam()
    {
        return sExam;
    }
    public void setsRemainderSemester(String sRemainderSemester)
    {
        this.sRemainderSemester = sRemainderSemester;
    }

    public String getsRemainderSemester()
    {
        return sRemainderSemester;
    }
    public void setsStatus(String sStatus)
    {
        this.sStatus = sStatus;
    }

    public String getsStatus()
    {
        return sStatus;
    }
    public void setsAttClassId(Long sAttClassId)
    {
        this.sAttClassId = sAttClassId;
    }

    public Long getsAttClassId()
    {
        return sAttClassId;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("sId", getsId())
            .append("sName", getsName())
            .append("sSex", getsSex())
            .append("sAge", getsAge())
            .append("sPhone", getsPhone())
            .append("sNumid", getsNumid())
            .append("sExam", getsExam())
            .append("sRemainderSemester", getsRemainderSemester())
            .append("sStatus", getsStatus())
            .append("sAttClassId", getsAttClassId())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("dataAttDept", getDataAttDept())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
