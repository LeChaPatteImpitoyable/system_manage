package com.ruoyi.student.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 学员信息对象 stu_info
 *
 * @author ruoyi
 * @date 2020-10-09
 */
public class StuInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 学员编号 */
    @Excel(name = "学员编号")
    private Long sId;

    /** 学员姓名 */
    @Excel(name = "学员姓名")
    private String sName;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String sPhone;

    /** 用户性别（0男 1女 2未知） */
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    private String sSex;

    /** 学员状态（0正常） */
    @Excel(name = "学员状态", readConverterExp = "0=正常")
    private String sStatus;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    private Long deptId;

    private List<String> labelStrList;

    public List<String> getLabelStrList() {
        return labelStrList;
    }

    public void setLabelStrList(List<String> labelStrList) {
        this.labelStrList = labelStrList;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public void setsId(Long sId)
    {
        this.sId = sId;
    }

    public Long getsId()
    {
        return sId;
    }
    public void setsName(String sName)
    {
        this.sName = sName;
    }

    public String getsName()
    {
        return sName;
    }
    public void setsPhone(String sPhone)
    {
        this.sPhone = sPhone;
    }

    public String getsPhone()
    {
        return sPhone;
    }
    public void setsSex(String sSex)
    {
        this.sSex = sSex;
    }

    public String getsSex()
    {
        return sSex;
    }
    public void setsStatus(String sStatus)
    {
        this.sStatus = sStatus;
    }

    public String getsStatus()
    {
        return sStatus;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("sId", getsId())
            .append("sName", getsName())
            .append("sPhone", getsPhone())
            .append("sSex", getsSex())
            .append("sStatus", getsStatus())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("dataAttDept", getDataAttDept())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
