package com.ruoyi.student.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.student.mapper.StuLabelMapper;
import com.ruoyi.student.domain.StuLabel;
import com.ruoyi.student.service.IStuLabelService;

/**
 * 学员标签Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-10-09
 */
@Service
public class StuLabelServiceImpl implements IStuLabelService 
{
    @Autowired
    private StuLabelMapper stuLabelMapper;

    /**
     * 查询学员标签
     * 
     * @param labelId 学员标签ID
     * @return 学员标签
     */
    @Override
    public StuLabel selectStuLabelById(Long labelId)
    {
        return stuLabelMapper.selectStuLabelById(labelId);
    }

    /**
     * 查询学员标签列表
     * 
     * @param stuLabel 学员标签
     * @return 学员标签
     */
    @Override
    public List<StuLabel> selectStuLabelList(StuLabel stuLabel)
    {
        return stuLabelMapper.selectStuLabelList(stuLabel);
    }

    /**
     * 新增学员标签
     * 
     * @param stuLabel 学员标签
     * @return 结果
     */
    @Override
    public int insertStuLabel(StuLabel stuLabel)
    {
        stuLabel.setCreateTime(DateUtils.getNowDate());
        return stuLabelMapper.insertStuLabel(stuLabel);
    }

    /**
     * 修改学员标签
     * 
     * @param stuLabel 学员标签
     * @return 结果
     */
    @Override
    public int updateStuLabel(StuLabel stuLabel)
    {
        stuLabel.setUpdateTime(DateUtils.getNowDate());
        return stuLabelMapper.updateStuLabel(stuLabel);
    }

    /**
     * 批量删除学员标签
     * 
     * @param labelIds 需要删除的学员标签ID
     * @return 结果
     */
    @Override
    public int deleteStuLabelByIds(Long[] labelIds)
    {
        return stuLabelMapper.deleteStuLabelByIds(labelIds);
    }

    /**
     * 删除学员标签信息
     * 
     * @param labelId 学员标签ID
     * @return 结果
     */
    @Override
    public int deleteStuLabelById(Long labelId)
    {
        return stuLabelMapper.deleteStuLabelById(labelId);
    }
}
