package com.ruoyi.student.service;

import java.util.List;
import com.ruoyi.student.domain.StuInfo;

/**
 * 学员信息Service接口
 * 
 * @author ruoyi
 * @date 2020-10-09
 */
public interface IStuInfoService 
{
    /**
     * 查询学员信息
     * 
     * @param sId 学员信息ID
     * @return 学员信息
     */
    public StuInfo selectStuInfoById(Long sId);

    /**
     * 查询学员信息列表
     * 
     * @param stuInfo 学员信息
     * @return 学员信息集合
     */
    public List<StuInfo> selectStuInfoList(StuInfo stuInfo);

    /**
     * 新增学员信息
     * 
     * @param stuInfo 学员信息
     * @return 结果
     */
    public int insertStuInfo(StuInfo stuInfo);

    /**
     * 修改学员信息
     * 
     * @param stuInfo 学员信息
     * @return 结果
     */
    public int updateStuInfo(StuInfo stuInfo);

    /**
     * 批量删除学员信息
     * 
     * @param sIds 需要删除的学员信息ID
     * @return 结果
     */
    public int deleteStuInfoByIds(Long[] sIds);

    /**
     * 删除学员信息信息
     * 
     * @param sId 学员信息ID
     * @return 结果
     */
    public int deleteStuInfoById(Long sId);
}
