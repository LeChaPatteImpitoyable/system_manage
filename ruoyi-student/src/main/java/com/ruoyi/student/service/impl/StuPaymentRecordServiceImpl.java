package com.ruoyi.student.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.student.mapper.StuPaymentRecordMapper;
import com.ruoyi.student.domain.StuPaymentRecord;
import com.ruoyi.student.service.IStuPaymentRecordService;

/**
 * 缴费记录Service业务层处理
 *
 * @author yingsy
 * @date 2020-10-16
 */
@Service
public class StuPaymentRecordServiceImpl implements IStuPaymentRecordService
{
    @Autowired
    private StuPaymentRecordMapper stuPaymentRecordMapper;

    /**
     * 查询缴费记录
     *
     * @param recordId 缴费记录ID
     * @return 缴费记录
     */
    @Override
    public StuPaymentRecord selectStuPaymentRecordById(Long recordId)
    {
        return stuPaymentRecordMapper.selectStuPaymentRecordById(recordId);
    }

    /**
     * 查询缴费记录列表
     *
     * @param stuPaymentRecord 缴费记录
     * @return 缴费记录
     */
    @Override
    public List<StuPaymentRecord> selectStuPaymentRecordList(StuPaymentRecord stuPaymentRecord)
    {
        return stuPaymentRecordMapper.selectStuPaymentRecordList(stuPaymentRecord);
    }

    /**
     * 新增缴费记录
     *
     * @param stuPaymentRecord 缴费记录
     * @return 结果
     */
    @Override
    public int insertStuPaymentRecord(StuPaymentRecord stuPaymentRecord)
    {
        return stuPaymentRecordMapper.insertStuPaymentRecord(stuPaymentRecord);
    }

    /**
     * 修改缴费记录
     *
     * @param stuPaymentRecord 缴费记录
     * @return 结果
     */
    @Override
    public int updateStuPaymentRecord(StuPaymentRecord stuPaymentRecord)
    {
        deleteStuPaymentRecordById(stuPaymentRecord.getUpdateBy(), stuPaymentRecord.getRecordId());
        return stuPaymentRecordMapper.insertStuPaymentRecord(stuPaymentRecord);
    }

    /**
     * 批量删除缴费记录
     *
     * @param recordIds 需要删除的缴费记录ID
     * @return 结果
     */
    @Override
    public int deleteStuPaymentRecordByIds(String updateBy, Long[] recordIds)
    {
        return stuPaymentRecordMapper.delStuPaymentRecordByIds(recordIds, updateBy);
    }

    /**
     * 删除缴费记录信息
     *
     * @param recordId 缴费记录ID
     * @return 结果
     */
    @Override
    public int deleteStuPaymentRecordById(String updateBy, Long recordId)
    {
        return stuPaymentRecordMapper.delStuPaymentRecordById(recordId, updateBy);
    }
}
