package com.ruoyi.student.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.DataAttScope;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.student.mapper.StuInfoMapper;
import com.ruoyi.student.domain.StuInfo;
import com.ruoyi.student.service.IStuInfoService;

/**
 * 学员信息Service业务层处理
 *
 * @author ruoyi
 * @date 2020-10-09
 */
@Service
public class StuInfoServiceImpl implements IStuInfoService
{
    @Autowired
    private StuInfoMapper stuInfoMapper;

    /**
     * 查询学员信息
     *
     * @param sId 学员信息ID
     * @return 学员信息
     */
    @Override
    public StuInfo selectStuInfoById(Long sId)
    {
        return stuInfoMapper.selectStuInfoById(sId);
    }

    /**
     * 查询学员信息列表
     *
     * @param stuInfo 学员信息
     * @return 学员信息
     */
    @Override
    @DataAttScope
    public List<StuInfo> selectStuInfoList(StuInfo stuInfo)
    {
        return stuInfoMapper.selectStuInfoList(stuInfo);
    }

    /**
     * 新增学员信息
     *
     * @param stuInfo 学员信息
     * @return 结果
     */
    @Override
    public int insertStuInfo(StuInfo stuInfo)
    {
        stuInfo.setCreateTime(DateUtils.getNowDate());
        return stuInfoMapper.insertStuInfo(stuInfo);
    }

    /**
     * 修改学员信息
     *
     * @param stuInfo 学员信息
     * @return 结果
     */
    @Override
    public int updateStuInfo(StuInfo stuInfo)
    {
        stuInfo.setUpdateTime(DateUtils.getNowDate());
        return stuInfoMapper.updateStuInfo(stuInfo);
    }

    /**
     * 批量删除学员信息
     *
     * @param sIds 需要删除的学员信息ID
     * @return 结果
     */
    @Override
    public int deleteStuInfoByIds(Long[] sIds)
    {
        return stuInfoMapper.deleteStuInfoByIds(sIds);
    }

    /**
     * 删除学员信息信息
     *
     * @param sId 学员信息ID
     * @return 结果
     */
    @Override
    public int deleteStuInfoById(Long sId)
    {
        return stuInfoMapper.deleteStuInfoById(sId);
    }
}
