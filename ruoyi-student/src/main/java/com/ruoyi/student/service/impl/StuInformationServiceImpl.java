package com.ruoyi.student.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.ClassAttScope;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.student.mapper.StuInformationMapper;
import com.ruoyi.student.domain.StuInformation;
import com.ruoyi.student.service.IStuInformationService;

/**
 * 学员信息Service业务层处理
 *
 * @author ying
 * @date 2020-10-15
 */
@Service
public class StuInformationServiceImpl implements IStuInformationService
{
    @Autowired
    private StuInformationMapper stuInformationMapper;

    /**
     * 查询学员信息
     *
     * @param sId 学员信息ID
     * @return 学员信息
     */
    @Override
    public StuInformation selectStuInformationById(Long sId)
    {
        return stuInformationMapper.selectStuInformationById(sId);
    }

    /**
     * 查询学员信息列表
     *
     * @param stuInformation 学员信息
     * @return 学员信息
     */
    @Override
    @ClassAttScope
    public List<StuInformation> selectStuInformationList(StuInformation stuInformation)
    {
        return stuInformationMapper.selectStuInformationList(stuInformation);
    }

    /**
     * 新增学员信息
     *
     * @param stuInformation 学员信息
     * @return 结果
     */
    @Override
    public int insertStuInformation(StuInformation stuInformation)
    {
        return stuInformationMapper.insertStuInformation(stuInformation);
    }

    /**
     * 修改学员信息
     *
     * @param stuInformation 学员信息
     * @return 结果
     */
    @Override
    public int updateStuInformation(StuInformation stuInformation)
    {
        deleteStuInformationById(stuInformation.getUpdateBy(), stuInformation.getsId());
        return insertStuInformation(stuInformation);
    }

    /**
     * 批量删除学员信息
     *
     * @param sIds 需要删除的学员信息ID
     * @return 结果
     */
    @Override
    public int deleteStuInformationByIds(String updateBy, Long[] sIds)
    {
        return stuInformationMapper.delStuInformationByIds(sIds, updateBy);
    }

    /**
     * 删除学员信息信息
     *
     * @param sId 学员信息ID
     * @return 结果
     */
    @Override
    public int deleteStuInformationById(String updateBy, Long sId)
    {
        return stuInformationMapper.delStuInformationById(sId, updateBy);
    }
}
