package com.ruoyi.student.mapper;

import java.util.List;
import com.ruoyi.student.domain.StuLabel;

/**
 * 学员标签Mapper接口
 * 
 * @author ruoyi
 * @date 2020-10-09
 */
public interface StuLabelMapper 
{
    /**
     * 查询学员标签
     * 
     * @param labelId 学员标签ID
     * @return 学员标签
     */
    public StuLabel selectStuLabelById(Long labelId);

    /**
     * 查询学员标签列表
     * 
     * @param stuLabel 学员标签
     * @return 学员标签集合
     */
    public List<StuLabel> selectStuLabelList(StuLabel stuLabel);

    /**
     * 新增学员标签
     * 
     * @param stuLabel 学员标签
     * @return 结果
     */
    public int insertStuLabel(StuLabel stuLabel);

    /**
     * 修改学员标签
     * 
     * @param stuLabel 学员标签
     * @return 结果
     */
    public int updateStuLabel(StuLabel stuLabel);

    /**
     * 删除学员标签
     * 
     * @param labelId 学员标签ID
     * @return 结果
     */
    public int deleteStuLabelById(Long labelId);

    /**
     * 批量删除学员标签
     * 
     * @param labelIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteStuLabelByIds(Long[] labelIds);
}
