package com.ruoyi.student.mapper;

import java.util.List;
import com.ruoyi.student.domain.StuInformation;
import org.apache.ibatis.annotations.Param;

/**
 * 学员信息Mapper接口
 *
 * @author ying
 * @date 2020-10-15
 */
public interface StuInformationMapper
{
    /**
     * 查询学员信息
     *
     * @param sId 学员信息ID
     * @return 学员信息
     */
    public StuInformation selectStuInformationById(Long sId);

    /**
     * 查询学员信息列表
     *
     * @param stuInformation 学员信息
     * @return 学员信息集合
     */
    public List<StuInformation> selectStuInformationList(StuInformation stuInformation);

    /**
     * 新增学员信息
     *
     * @param stuInformation 学员信息
     * @return 结果
     */
    public int insertStuInformation(StuInformation stuInformation);

    /**
     * 修改学员信息
     *
     * @param stuInformation 学员信息
     * @return 结果
     */
    public int updateStuInformation(StuInformation stuInformation);

    /**
     * 删除学员信息
     *
     * @param sId 学员信息ID
     * @return 结果
     */
    public int deleteStuInformationById(Long sId);
    public int delStuInformationById(@Param("sId") Long sId,@Param("updateBy") String updateBy);

    /**
     * 批量删除学员信息
     *
     * @param sIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteStuInformationByIds(Long[] sIds);
    public int delStuInformationByIds(@Param("sIds") Long[] sIds,@Param("updateBy") String updateBy);
}
