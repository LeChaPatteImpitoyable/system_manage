package com.ruoyi.student.mapper;

import java.util.List;
import com.ruoyi.student.domain.StuPaymentRecord;
import org.apache.ibatis.annotations.Param;

/**
 * 缴费记录Mapper接口
 *
 * @author yingsy
 * @date 2020-10-16
 */
public interface StuPaymentRecordMapper
{
    /**
     * 查询缴费记录
     *
     * @param recordId 缴费记录ID
     * @return 缴费记录
     */
    public StuPaymentRecord selectStuPaymentRecordById(Long recordId);

    /**
     * 查询缴费记录列表
     *
     * @param stuPaymentRecord 缴费记录
     * @return 缴费记录集合
     */
    public List<StuPaymentRecord> selectStuPaymentRecordList(StuPaymentRecord stuPaymentRecord);

    /**
     * 新增缴费记录
     *
     * @param stuPaymentRecord 缴费记录
     * @return 结果
     */
    public int insertStuPaymentRecord(StuPaymentRecord stuPaymentRecord);

    /**
     * 修改缴费记录
     *
     * @param stuPaymentRecord 缴费记录
     * @return 结果
     */
    public int updateStuPaymentRecord(StuPaymentRecord stuPaymentRecord);

    /**
     * 删除缴费记录
     *
     * @param recordId 缴费记录ID
     * @return 结果
     */
    public int deleteStuPaymentRecordById(Long recordId);
    public int delStuPaymentRecordById(@Param("recordId") Long recordId, @Param("updateBy") String updateBy);

    /**
     * 批量删除缴费记录
     *
     * @param recordIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteStuPaymentRecordByIds(Long[] recordIds);
    public int delStuPaymentRecordByIds(@Param("recordIds") Long[] recordIds,@Param("updateBy") String updateBy);
}
