var  path = require( 'path' )

module.exports = {
    build: {
        proxyTable: {
            [process.env.VUE_APP_BASE_API]: {
                target: `http://localhost:8080/ruoyi`,
                changeOrigin: true,
                pathRewrite: {
                    ['^' + process.env.VUE_APP_BASE_API]: ''
                }
            }
        }
    }
}
