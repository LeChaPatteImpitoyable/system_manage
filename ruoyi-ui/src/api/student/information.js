import request from '@/utils/request'

// 查询学员信息列表
export function listInformation(query) {
  return request({
    url: '/student/information/list',
    method: 'get',
    params: query
  })
}

// 查询学员信息详细
export function getInformation(sId) {
  return request({
    url: '/student/information/' + sId,
    method: 'get'
  })
}

// 新增学员信息
export function addInformation(data) {
  return request({
    url: '/student/information',
    method: 'post',
    data: data
  })
}

// 修改学员信息
export function updateInformation(data) {
  return request({
    url: '/student/information',
    method: 'put',
    data: data
  })
}

// 删除学员信息
export function delInformation(sId) {
  return request({
    url: '/student/information/' + sId,
    method: 'delete'
  })
}

// 导出学员信息
export function exportInformation(query) {
  return request({
    url: '/student/information/export',
    method: 'get',
    params: query
  })
}