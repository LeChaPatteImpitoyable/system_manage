import request from '@/utils/request'

// 查询学员信息列表
export function listInfo(query) {
  return request({
    url: '/student/info/list',
    method: 'get',
    params: query
  })
}

// 查询学员信息详细
export function getInfo(sId) {
  return request({
    url: '/student/info/' + sId,
    method: 'get'
  })
}

// 新增学员信息
export function addInfo(data) {
  return request({
    url: '/student/info',
    method: 'post',
    data: data
  })
}

// 修改学员信息
export function updateInfo(data) {
  return request({
    url: '/student/info',
    method: 'put',
    data: data
  })
}

// 删除学员信息
export function delInfo(sId) {
  return request({
    url: '/student/info/' + sId,
    method: 'delete'
  })
}

// 导出学员信息
export function exportInfo(query) {
  return request({
    url: '/student/info/export',
    method: 'get',
    params: query
  })
}