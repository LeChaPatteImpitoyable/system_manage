import request from '@/utils/request'

// 查询学员标签列表
export function listLabel(query) {
  return request({
    url: '/student/label/list',
    method: 'get',
    params: query
  })
}

// 查询学员标签详细
export function getLabel(labelId) {
  return request({
    url: '/student/label/' + labelId,
    method: 'get'
  })
}

// 新增学员标签
export function addLabel(data) {
  return request({
    url: '/student/label',
    method: 'post',
    data: data
  })
}

// 修改学员标签
export function updateLabel(data) {
  return request({
    url: '/student/label',
    method: 'put',
    data: data
  })
}

// 删除学员标签
export function delLabel(labelId) {
  return request({
    url: '/student/label/' + labelId,
    method: 'delete'
  })
}

// 导出学员标签
export function exportLabel(query) {
  return request({
    url: '/student/label/export',
    method: 'get',
    params: query
  })
}