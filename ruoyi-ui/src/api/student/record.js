import request from '@/utils/request'

// 查询缴费记录列表
export function listRecord(query) {
  return request({
    url: '/student/record/list',
    method: 'get',
    params: query
  })
}

// 查询缴费记录详细
export function getRecord(recordId) {
  return request({
    url: '/student/record/' + recordId,
    method: 'get'
  })
}

// 新增缴费记录
export function addRecord(data) {
  return request({
    url: '/student/record',
    method: 'post',
    data: data
  })
}

// 修改缴费记录
export function updateRecord(data) {
  return request({
    url: '/student/record',
    method: 'put',
    data: data
  })
}

// 删除缴费记录
export function delRecord(recordId) {
  return request({
    url: '/student/record/' + recordId,
    method: 'delete'
  })
}

// 导出缴费记录
export function exportRecord(query) {
  return request({
    url: '/student/record/export',
    method: 'get',
    params: query
  })
}