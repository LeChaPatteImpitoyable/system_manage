package com.ruoyi.web.controller.student;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.student.domain.StuPaymentRecord;
import com.ruoyi.student.service.IStuPaymentRecordService;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.web.controller.common.BusinessBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 缴费记录Controller
 *
 * @author yingsy
 * @date 2020-10-16
 */
@RestController
@RequestMapping("/student/record")
public class StuPaymentRecordController extends BusinessBaseController
{
    @Autowired
    private IStuPaymentRecordService stuPaymentRecordService;

    /**
     * 查询缴费记录列表
     */
    @PreAuthorize("@ss.hasPermi('student:record:list')")
    @GetMapping("/list")
    public TableDataInfo list(StuPaymentRecord stuPaymentRecord)
    {
        startPage();
        List<StuPaymentRecord> list = stuPaymentRecordService.selectStuPaymentRecordList(stuPaymentRecord);
        return getDataTable(list);
    }

    /**
     * 导出缴费记录列表
     */
    @PreAuthorize("@ss.hasPermi('student:record:export')")
    @Log(title = "缴费记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StuPaymentRecord stuPaymentRecord)
    {
        List<StuPaymentRecord> list = stuPaymentRecordService.selectStuPaymentRecordList(stuPaymentRecord);
        ExcelUtil<StuPaymentRecord> util = new ExcelUtil<StuPaymentRecord>(StuPaymentRecord.class);
        return util.exportExcel(list, "record");
    }

    /**
     * 获取缴费记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('student:record:query')")
    @GetMapping(value = "/{recordId}")
    public AjaxResult getInfo(@PathVariable("recordId") Long recordId)
    {
        return AjaxResult.success(stuPaymentRecordService.selectStuPaymentRecordById(recordId));
    }

    /**
     * 新增缴费记录
     */
    @PreAuthorize("@ss.hasPermi('student:record:add')")
    @Log(title = "缴费记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StuPaymentRecord stuPaymentRecord)
    {
        fillBaseEntity(stuPaymentRecord);
        return toAjax(stuPaymentRecordService.insertStuPaymentRecord(stuPaymentRecord));
    }


    /**
     * 修改缴费记录
     */
    @PreAuthorize("@ss.hasPermi('student:record:edit')")
    @Log(title = "缴费记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StuPaymentRecord stuPaymentRecord)
    {
        fillBaseEntity(stuPaymentRecord);
        return toAjax(stuPaymentRecordService.updateStuPaymentRecord(stuPaymentRecord));
    }

    /**
     * 删除缴费记录
     */
    @PreAuthorize("@ss.hasPermi('student:record:remove')")
    @Log(title = "缴费记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recordIds}")
    public AjaxResult remove(@PathVariable Long[] recordIds)
    {
        return toAjax(stuPaymentRecordService.deleteStuPaymentRecordByIds(getUsreId(), recordIds));
    }
}
