package com.ruoyi.web.controller.student;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.student.domain.StuInfo;
import com.ruoyi.student.service.IStuInfoService;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.web.controller.common.BusinessBaseController;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 学员信息Controller
 *
 * @author ruoyi
 * @date 2020-10-09
 */
@RestController
@RequestMapping("/student/info")
public class StuInfoController extends BusinessBaseController
{
    @Autowired
    private IStuInfoService stuInfoService;

    /**
     * 查询学员信息列表
     */
    @PreAuthorize("@ss.hasPermi('student:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(StuInfo stuInfo)
    {
        startPage();
        List<StuInfo> list = stuInfoService.selectStuInfoList(stuInfo);
        if(CollectionUtils.isNotEmpty(list)){
            list.stream().forEach(obj ->{
                List<String> lableList = new ArrayList<>();
                lableList.add("a");
                lableList.add("b");
                lableList.add("c");
                obj.setLabelStrList(lableList);
            });
        }
        return getDataTable(list);
    }

    /**
     * 导出学员信息列表
     */
    @PreAuthorize("@ss.hasPermi('student:info:export')")
    @Log(title = "学员信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StuInfo stuInfo)
    {
        List<StuInfo> list = stuInfoService.selectStuInfoList(stuInfo);
        ExcelUtil<StuInfo> util = new ExcelUtil<StuInfo>(StuInfo.class);
        return util.exportExcel(list, "info");
    }

    /**
     * 获取学员信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('student:info:query')")
    @GetMapping(value = "/{sId}")
    public AjaxResult getInfo(@PathVariable("sId") Long sId)
    {
        StuInfo stuInfo = stuInfoService.selectStuInfoById(sId);
        List<String> lableList = new ArrayList<>();
        lableList.add("a");
        lableList.add("b");
        lableList.add("c");
        stuInfo.setLabelStrList(lableList);
        return AjaxResult.success(stuInfo);
    }

    /**
     * 新增学员信息
     */
    @PreAuthorize("@ss.hasPermi('student:info:add')")
    @Log(title = "学员信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StuInfo stuInfo)
    {
        fillBaseEntity(stuInfo);
        return toAjax(stuInfoService.insertStuInfo(stuInfo));
    }

    /**
     * 修改学员信息
     */
    @PreAuthorize("@ss.hasPermi('student:info:edit')")
    @Log(title = "学员信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StuInfo stuInfo)
    {
        fillBaseEntity(stuInfo);
        return toAjax(stuInfoService.updateStuInfo(stuInfo));
    }

    /**
     * 删除学员信息
     */
    @PreAuthorize("@ss.hasPermi('student:info:remove')")
    @Log(title = "学员信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{sIds}")
    public AjaxResult remove(@PathVariable Long[] sIds)
    {
        return toAjax(stuInfoService.deleteStuInfoByIds(sIds));
    }
}
