package com.ruoyi.web.controller.common;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.service.ISysDeptService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by yingsy on 2020/10/17.
 */
public class BusinessBaseController extends BaseController {

    @Autowired
    protected TokenService tokenService;

    @Autowired
    protected ISysDeptService deptService;

    public void fillBaseEntity(BaseEntity baseEntity){
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        baseEntity.setCreateBy(String.valueOf(loginUser.getUser().getUserId()));
        baseEntity.setUpdateBy(String.valueOf(loginUser.getUser().getUserId()));
        SysDept sysDept = deptService.selectDeptById(loginUser.getUser().getDeptId());
        if(null != sysDept){
            if(null != sysDept.getParentId()){
                baseEntity.setDataAttDept(sysDept.getAncestors()+","+sysDept.getDeptId());
            }else{
                baseEntity.setDataAttDept(sysDept.getAncestors());
            }
        }else{
            baseEntity.setDataAttDept(String.valueOf(loginUser.getUser().getDeptId()));
        }
    }

    public String getUsreId(){
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        return String.valueOf(loginUser.getUser().getUserId());
    }
}
