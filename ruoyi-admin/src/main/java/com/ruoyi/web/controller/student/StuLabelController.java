package com.ruoyi.web.controller.student;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.student.domain.StuLabel;
import com.ruoyi.student.service.IStuLabelService;
import com.ruoyi.web.controller.common.BusinessBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 学员标签Controller
 *
 * @author ruoyi
 * @date 2020-10-09
 */
@RestController
@RequestMapping("/student/label")
public class StuLabelController extends BusinessBaseController
{
    @Autowired
    private IStuLabelService stuLabelService;

    /**
     * 查询学员标签列表
     */
    @PreAuthorize("@ss.hasPermi('student:label:list')")
    @GetMapping("/list")
    public TableDataInfo list(StuLabel stuLabel)
    {
        startPage();
        List<StuLabel> list = stuLabelService.selectStuLabelList(stuLabel);
        return getDataTable(list);
    }

    /**
     * 导出学员标签列表
     */
    @PreAuthorize("@ss.hasPermi('student:label:export')")
    @Log(title = "学员标签", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StuLabel stuLabel)
    {
        List<StuLabel> list = stuLabelService.selectStuLabelList(stuLabel);
        ExcelUtil<StuLabel> util = new ExcelUtil<StuLabel>(StuLabel.class);
        return util.exportExcel(list, "label");
    }

    /**
     * 获取学员标签详细信息
     */
    @PreAuthorize("@ss.hasPermi('student:label:query')")
    @GetMapping(value = "/{labelId}")
    public AjaxResult getInfo(@PathVariable("labelId") Long labelId)
    {
        return AjaxResult.success(stuLabelService.selectStuLabelById(labelId));
    }

    /**
     * 新增学员标签
     */
    @PreAuthorize("@ss.hasPermi('student:label:add')")
    @Log(title = "学员标签", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StuLabel stuLabel)
    {
        fillBaseEntity(stuLabel);
        return toAjax(stuLabelService.insertStuLabel(stuLabel));
    }

    /**
     * 修改学员标签
     */
    @PreAuthorize("@ss.hasPermi('student:label:edit')")
    @Log(title = "学员标签", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StuLabel stuLabel)
    {
        fillBaseEntity(stuLabel);
        return toAjax(stuLabelService.updateStuLabel(stuLabel));
    }

    /**
     * 删除学员标签
     */
    @PreAuthorize("@ss.hasPermi('student:label:remove')")
    @Log(title = "学员标签", businessType = BusinessType.DELETE)
	@DeleteMapping("/{labelIds}")
    public AjaxResult remove(@PathVariable Long[] labelIds)
    {
        return toAjax(stuLabelService.deleteStuLabelByIds(labelIds));
    }
}
