package com.ruoyi.web.controller.student;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.student.domain.StuInformation;
import com.ruoyi.student.service.IStuInformationService;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.web.controller.common.BusinessBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 学员信息Controller
 *
 * @author ying
 * @date 2020-10-15
 */
@RestController
@RequestMapping("/student/information")
public class StuInformationController extends BusinessBaseController
{
    @Autowired
    private IStuInformationService stuInformationService;

    /**
     * 查询学员信息列表
     */
    @PreAuthorize("@ss.hasPermi('student:information:list')")
    @GetMapping("/list")
    public TableDataInfo list(StuInformation stuInformation)
    {
        startPage();
        List<StuInformation> list = stuInformationService.selectStuInformationList(stuInformation);
        return getDataTable(list);
    }

    /**
     * 导出学员信息列表
     */
    @PreAuthorize("@ss.hasPermi('student:information:export')")
    @Log(title = "学员信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StuInformation stuInformation)
    {
        List<StuInformation> list = stuInformationService.selectStuInformationList(stuInformation);
        ExcelUtil<StuInformation> util = new ExcelUtil<StuInformation>(StuInformation.class);
        return util.exportExcel(list, "information");
    }

    /**
     * 获取学员信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('student:information:query')")
    @GetMapping(value = "/{sId}")
    public AjaxResult getInfo(@PathVariable("sId") Long sId)
    {
        return AjaxResult.success(stuInformationService.selectStuInformationById(sId));
    }

    /**
     * 新增学员信息
     */
    @PreAuthorize("@ss.hasPermi('student:information:add')")
    @Log(title = "学员信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StuInformation stuInformation)
    {
        fillBaseEntity(stuInformation);
        return toAjax(stuInformationService.insertStuInformation(stuInformation));
    }

    /**
     * 修改学员信息
     */
    @PreAuthorize("@ss.hasPermi('student:information:edit')")
    @Log(title = "学员信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StuInformation stuInformation)
    {
        fillBaseEntity(stuInformation);
        return toAjax(stuInformationService.updateStuInformation(stuInformation));
    }

    /**
     * 删除学员信息
     */
    @PreAuthorize("@ss.hasPermi('student:information:remove')")
    @Log(title = "学员信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{sIds}")
    public AjaxResult remove(@PathVariable Long[] sIds)
    {
        return toAjax(stuInformationService.deleteStuInformationByIds(getUsreId(), sIds));
    }
}
